/*EJEMPLO DE TRANSACCIONES*/

BEGIN TRAN  --Comando para Iniciar una Transacci�n
BEGIN TRY
 SELECT * FROM marca
 UPDATE marca SET descripcion='EN MAY�SCULA' where id=1003
 SELECT * FROM marca
 DELETE FROM marca where id=1003
 SELECT * FROM marca
 DECLARE @a INT
 SET @a = 1/0
 PRINT 'Imprimir'
 COMMIT  --Comando para Guardar o Confirmar la Transacci�n
END TRY
BEGIN CATCH
	--PRINT 'SUCEDI� UN ERROR'
	ROLLBACK  --Comando para Revertir una transacci�n
	SELECT ERROR_MESSAGE()
END CATCH
SELECT * FROM marca
--EXEC marca_consultar
