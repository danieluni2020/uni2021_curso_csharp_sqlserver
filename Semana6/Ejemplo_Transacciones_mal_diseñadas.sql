
BEGIN TRAN  --Iniciamos Transacci�n
BEGIN TRY   --Inicio Bloque Try
	EXEC marca_guardar 0,'PRUEBA2'  --Insertamos
	IF @@ROWCOUNT>1  --Mala l�gica
		COMMIT       --Commit Nunca Accesible, porque insert s�lo afect� un registro
END TRY    
BEGIN CATCH
	ROLLBACK   --Revertir Cambios
END CATCH

SELECT * FROM marca
SELECT @@TRANCOUNT  --Cuenta cantidad de Transacciones para esta sesi�n
ROLLBACK