﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Semana5EjemploHerencia
{
    class Sumar3 : Operacion3
    {
        public Sumar3(int v1,int v2) 
            : base(v1, v2)
        {

        }

        public void Operar()
        {
            this.Resultado = this.Valor1 + this.Valor2;
        }
    }
}
