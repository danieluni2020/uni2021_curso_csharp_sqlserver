﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Semana5EjemploHerencia
{
    class Suma : Operaciones
    {
        public Suma(int v1,int v2)
        {
            this.v1 = v1;
            this.v2 = v2;
        }
        public int Operar()
        {
            return this.v1 + this.v2;
        }
    }
}
