﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Semana5EjemploHerencia
{
    class Sumar2 : Operacion2
    {
        public void Operar()
        {
            this.Resultado = this.Valor1 + this.Valor2;
        }
    }
}
