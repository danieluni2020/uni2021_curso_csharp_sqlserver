﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Semana5EjemploHerencia
{
    class Operacion2
    {
        private int _valor1;
        private int _valor2;
        private int resultado;

        public int Valor1 { get; set;  }
        public int Valor2 { get; set; }
        public int Resultado
        {
            protected set
            {
                this.resultado = value;
            }
            get
            {
                return this.resultado;
            }
        }
    }
}
