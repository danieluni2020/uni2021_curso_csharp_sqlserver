﻿using System;

namespace Semana5EjemploHerencia
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Suma s1 = new Suma(23,41);
            Restar r1 = new Restar(74, 32);
            Console.WriteLine("La Suma es {0}", s1.Operar());
            Console.WriteLine("La Resta es {0}", r1.Operar());
            */
            /*int valor1, valor2, resultado;
            Console.Write("Valor 1 = ");
            valor1 = int.Parse(Console.ReadLine());
            Console.Write("Valor 2 = ");
            valor2 = int.Parse(Console.ReadLine());
            Suma s1 = new Suma(valor1,valor2);
            Restar r1 = new Restar(valor1, valor2);
            resultado = s1.Operar();
            Console.WriteLine("La Suma es {0}", resultado);
            resultado = r1.Operar();
            Console.WriteLine("La Resta es {0}", resultado);
            */
            /*Sumar2 s2 = new Sumar2();
            s2.Valor1 = 45;
            s2.Valor2 = 85;
            //s2.Resultado = 85;
            s2.Operar();
            Console.WriteLine("{0}", s2.Resultado);
            */
            /*Sumar3 s3 = new Sumar3(85, 52);
            s3.Operar();
            //s3.Resultado = 41;
            Console.WriteLine("{0}",s3.Resultado);*/
            //Manejo de Excepciones Básico
            int edad;
            try
            {
                string edad_desde_usuario;
                Console.Write("Su Edad : ");
                edad_desde_usuario = Console.ReadLine();
                edad = int.Parse(edad_desde_usuario);
                Console.WriteLine("Su edad es: {0}", edad);
                float prom = 22 / edad;
            }
            catch (FormatException)
            {
                Console.WriteLine("Digitó un valor no numérico");
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Edad No puede ser CERO");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Sucedió error inesperado");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Gracias por Usar la App");
            }


            Console.ReadKey();
            
        }
    }
}
