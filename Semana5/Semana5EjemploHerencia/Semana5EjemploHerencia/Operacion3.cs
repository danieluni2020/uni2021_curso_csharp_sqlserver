﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Semana5EjemploHerencia
{
    class Operacion3
    {
        protected int Valor1 { get; set; }
        protected int Valor2 { get; set; }
        private int resultado;
        public int Resultado
        {
            protected set
            {
                this.resultado = value;
            }
            get
            {
                return this.resultado;
            }
        }

        public Operacion3(int v1,int v2)
        {
            this.Valor1 = v1;
            this.Valor2 = v2;
        }
    }
}
