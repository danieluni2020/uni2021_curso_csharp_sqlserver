﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Semana5EjemploHerencia
{
    class Restar : Operaciones
    {
        public Restar(int v1, int v2)
        {
            this.v1 = v1;
            this.v2 = v2;
        }
        public int Operar()
        {
            return this.v1 - this.v2;
        }
    }
}
