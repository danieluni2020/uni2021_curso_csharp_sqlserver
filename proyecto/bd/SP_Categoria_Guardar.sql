USE [proyecto1]
GO
/****** Object:  StoredProcedure [dbo].[categoria_guardar]    Script Date: 10/07/2021 10:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Creado:  20210605-1128
Modificado: 
Creado Por:  Daniel Bojorge
Objectivo: Guardar Categorìa
*/
ALTER PROCEDURE [dbo].[categoria_guardar](
	@id int = -1 output,
	@desc nvarchar(50),
	@activo bit = 1
) AS
BEGIN
	SET @desc = UPPER(@desc)
	DECLARE @tmp int
	SELECT @tmp = id FROM categoria WHERE id=@id
	IF @tmp IS NOT NULL
	BEGIN
		UPDATE categoria SET descripcion=@desc,activo=@activo,
			fecha_modifica = GETDATE()
			WHERE id=@id
	END
	ELSE
	BEGIN
		INSERT categoria (descripcion,activo) VALUES (@desc,@activo)
		SET @tmp = @@IDENTITY
		set @id = @tmp
	END
	SELECT @tmp
END
