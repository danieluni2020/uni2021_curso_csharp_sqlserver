USE proyecto1;
GO

CREATE TABLE Usuarios(
	username nvarchar(20) not null primary key,
	pass nvarchar(50) not null,
	nombre nvarchar(200),
	apellido nvarchar(200)
)
GO
INSERT INTO Usuarios VALUES
	('admin','123','Administrador',null),
	('usuario1','321','Prueba','Uno')
GO
CREATE PROCEDURE Login (
 @username nvarchar(20),
 @pass nvarchar(50)
) as
BEGIN
	SELECT * FROM Usuarios WHERE username=@username AND pass=@pass
END
GO
exec login 'admin','123'