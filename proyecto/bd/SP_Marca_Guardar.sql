USE [proyecto1]
GO
/****** Object:  StoredProcedure [dbo].[marca_guardar]    Script Date: 26/06/2021 8:08:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Creado:  20210605-1000
Modificado: 20210626-0808
Creado Por:  Daniel Bojorge
Objectivo: Guardar Marca
*/
ALTER PROCEDURE [dbo].[marca_guardar](
	@id int = 0 OUTPUT,
	@desc nvarchar(50)
) AS
BEGIN
	/*Buscar el Id en la tabla
	Si el id existe (existe el registro) entonces se actualiza el registro
	sino existe (no existe el registro) entonces se inserta el registro
	*/
	BEGIN TRAN
	BEGIN TRY
		SET @desc = UPPER(@desc)
		DECLARE @tmp int
		SELECT @tmp = ID from marca where id=@id
		IF @tmp IS NULL
		BEGIN
			INSERT INTO marca (descripcion) VALUES(@desc)
			--SET @tmp = @@IDENTITY
			SET @id = @@IDENTITY
			--SET @id = IDENT_CURRENT('Marca')
		END
		ELSE
		BEGIN
			UPDATE marca SET descripcion = @desc WHERE id=@id
		END
		--SELECT @tmp
		COMMIT
	END TRY
	BEGIN CATCH
		PRINT 'Hubo un ERROR'
		ROLLBACK
		SELECT ERROR_MESSAGE()
	END CATCH
END
GO

DECLARE @idTmp int
SET @idTmp = 0
--EXEC marca_consultar
--Ejecución SP con parámetro de salida
EXEC marca_guardar @idTmp OUTPUT,'SUMSUN'
SELECT @idTmp

