--Proyecto
--Crear Base de Datos
--DML, DDL, DCL
--DML:  Data Managment  Language  - Lenguaje de Manejo de Datos
--DDL:  Data Definition  Langauge   - Lenguaje de Definición de Datos
--DCL:  Data Control Language - Lenguaje de Control de Datos
--CREATE DATABASE proyecto1  --DDL
GO
use proyecto1
GO

CREATE TABLE marca(
	id int identity(1,1) primary key not null,
	descripcion nvarchar(50) not null UNIQUE
)

GO
/*
Operaciones CRUD - Create Read Update Delete
NOMBRETABLA_guardar  - Create (Insert) Update 
NOMBRETABLA_consultar - Select (1 Registro o Todos)
NOMBRETABLA_borrar  
*/

/*
Creado:  20210605-1000
Modificado: 20210605-1030
Creado Por:  Daniel Bojorge
Objectivo: Guardar Marca
*/
ALTER PROCEDURE marca_guardar(
	@id int = 0,
	@desc nvarchar(50)
) AS
BEGIN
	/*Buscar el Id en la tabla
	Si el id existe (existe el registro) entonces se actualiza el registro
	sino existe (no existe el registro) entonces se inserta el registro
	*/
	SET @desc = UPPER(@desc)
	DECLARE @tmp int
	SELECT @tmp = ID from marca where id=@id
	IF @tmp IS NULL
	BEGIN
		INSERT INTO marca (descripcion) VALUES(@desc)
		SET @tmp = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE marca SET descripcion = @desc WHERE id=@id
	END
	SELECT @tmp
END
GO
EXEC marca_guardar 0,'ibm1'
select * from marca
EXEC marca_guardar 1,'ibm'
select * from marca

EXEC marca_guardar -1,'lenovo524'
select * from marca
EXEC marca_guardar 2,'lenovo'
select * from marca

EXEC marca_guardar 0,'samsun'
select * from marca
EXEC marca_guardar 3,'samsung'
select * from marca
go

/*
Creado:  20210605-1040
Modificado: 
Creado Por:  Daniel Bojorge
Objectivo: Devolver Marca
*/
ALTER PROCEDURE marca_consultar(
	@id int = -1
) AS
BEGIN
	IF @ID=-1
	BEGIN
		SELECT * FROM marca ORDER BY id 
	END
	ELSE
	BEGIN
		SELECT * FROM marca WHERE id=@id
	END
END
GO
EXEC marca_consultar
EXEC marca_consultar 3
GO

/*
Creado:  20210605-1040
Modificado: 
Creado Por:  Daniel Bojorge
Objectivo: Devolver Marca
*/
CREATE PROCEDURE marca_borrar(@id int) AS
BEGIN
	DELETE FROM marca WHERE id=@id
END

EXEC marca_borrar
EXEC marca_borrar 3
EXEC marca_consultar
EXEC marca_guardar -1,'canon'
EXEC marca_consultar
