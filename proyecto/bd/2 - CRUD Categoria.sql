use proyecto1
go


--Crear Tabla Categoria
CREATE TABLE categoria(
	id int identity(1,1) primary key not null,
	descripcion nvarchar(50) not null unique,
	activo bit default 1,
	usuario_crea nvarchar(50),
	fecha_crea datetime default getdate(),
	usuario_modifica nvarchar(50),
	fecha_modifica datetime
)
GO

/*
Creado:  20210605-1128
Modificado: 
Creado Por:  Daniel Bojorge
Objectivo: Guardar Categor�a
*/
CREATE PROCEDURE categoria_guardar(
	@id int = -1,
	@desc nvarchar(50),
	@activo bit = 1
) AS
BEGIN
	SET @desc = UPPER(@desc)
	DECLARE @tmp int
	SELECT @tmp = id FROM categoria WHERE id=@id
	IF @tmp IS NOT NULL
	BEGIN
		UPDATE categoria SET descripcion=@desc,activo=@activo,
			fecha_modifica = GETDATE()
			WHERE id=@id
	END
	ELSE
	BEGIN
		INSERT categoria (descripcion,activo) VALUES (@desc,@activo)
		SET @tmp = @@IDENTITY
	END
	SELECT @tmp
END
GO

EXEC categoria_guardar -1,'Programaci�n'
select * from categoria
EXEC categoria_guardar 1,'Programacion'
select * from categoria
GO
/*
Creado:  20210605-1143
Modificado: 
Creado Por:  Daniel Bojorge
Objectivo: Devolver Categoria
*/
CREATE PROCEDURE categoria_consultar(
	@id int = -1
) AS
BEGIN
	IF @ID=-1
	BEGIN
		SELECT * FROM categoria ORDER BY id 
	END
	ELSE
	BEGIN
		SELECT * FROM categoria WHERE id=@id
	END
END
GO
EXEC categoria_consultar
EXEC categoria_consultar 1
GO

/*
Creado:  20210605-1144
Modificado: 
Creado Por:  Daniel Bojorge
Objectivo: Devolver Categoria
*/
CREATE PROCEDURE categoria_inactivar(@id int) AS
BEGIN
	UPDATE categoria SET 
		activo = 0,
		fecha_modifica = GETDATE()
	WHERE id=@id
END
GO
EXEC categoria_inactivar 1
EXEC categoria_consultar
EXEC categoria_guardar 1,'PROGRAMACION',1
EXEC categoria_consultar