﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Entidades;

using Datos;
namespace Negocios
{
    public class N_Marca
    {
        D_Marca dm = new D_Marca();
        
        public DataSet GetAll()
        {
            return dm.GetAll();
        }

        public E_Marca GetOne(int id)
        {
            return dm.GetOne(id);
        }
        public int Save(E_Marca o)
        {
            return dm.Save(o);
        }
        public Boolean Delete(int id)
        {
            return dm.Delete(id);
        }
    }
}
