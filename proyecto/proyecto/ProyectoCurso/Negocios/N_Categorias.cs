﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using Datos;
using Entidades;
namespace Negocios
{
    public class N_Categorias
    {
        D_Categorias dc = new D_Categorias();

        public DataSet GetAll()
        {
            return dc.GetAll();
        }

        public E_Categorias GetOne(int id)
        {
            return dc.GetOne(id);
        }

        public int Save(E_Categorias o)
        {
            return dc.Save(o);
        }
    }
}
