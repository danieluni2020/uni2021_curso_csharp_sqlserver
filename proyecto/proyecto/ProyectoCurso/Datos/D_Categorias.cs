﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using Entidades;

namespace Datos
{
    public class D_Categorias : CAD
    {
        public DataSet GetAll()
        {
            DataSet ds;
            try
            {
                SqlCommand cmd = CrearComando("categoria_consultar");
                ds = GetDS(cmd, "categoria_consultar");
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ds;
        }

        public E_Categorias GetOne(int id)
        {
            E_Categorias obj = new E_Categorias();
            SqlCommand cmd = CrearComando("categoria_consultar");
            try
            {
                cmd.Parameters.AddWithValue("@id", id);
                AbrirConexion();
                SqlDataReader r = Ejecuta_Consulta(cmd);
                if (r.Read())
                {
                    if (r.HasRows)
                    {
                        obj.Id = (int)r["id"];
                        obj.Descripcion = (string)r["descripcion"];
                        obj.Activo = (bool)r["activo"];
                        //obj.Usuario_Crea = (string)r["usuario_crea"];
                        //obj.Usuario_Modifica = (string)r["usuario_modifica"];
                    }
                    else
                    {
                        throw new Exception("Categorìa NO EXISTE");
                    }
                }
                else
                {
                    throw new Exception("Categoría Solicitada NO Existe");
                }
                r.Close();
                r.Dispose();
            }catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Dispose();
                CerrarConexion();
            }
            return obj;
        }

        public int Save(E_Categorias o)
        {
            int id = -1;
            SqlCommand cmd = CrearComando("categoria_guardar");
            try
            {
                cmd.Parameters.AddWithValue("@id", o.Id).Direction = ParameterDirection.InputOutput;
                cmd.Parameters.AddWithValue("@desc", o.Descripcion);
                cmd.Parameters.AddWithValue("@activo", o.Activo);

                AbrirConexion();
                Ejecuta_Accion(ref cmd);
                id = (int)cmd.Parameters["@id"].Value;
            }catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Dispose();
                CerrarConexion();
            }
            return id;
        }
        
    }
}
