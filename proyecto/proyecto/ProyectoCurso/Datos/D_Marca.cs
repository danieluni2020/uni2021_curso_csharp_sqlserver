﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using Entidades;

namespace Datos
{
    public class D_Marca : CAD
    {
        public DataSet GetAll()
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = CrearComando("marca_consultar");
                ds = GetDS(cmd, "marca_consultar");
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }

            return ds;
        }
        public E_Marca GetOne(int id)
        {
            E_Marca obj = new E_Marca();
            SqlCommand cmd = CrearComando("marca_consultar");
            try
            {
                cmd.Parameters.AddWithValue("@id", id);
                AbrirConexion();
                SqlDataReader respuesta = Ejecuta_Consulta(cmd);
                if (respuesta.Read())
                {
                    if (respuesta.HasRows)
                    {
                        obj.Id = (int)respuesta["id"];
                        obj.Descripcion = (string)respuesta["descripcion"];
                    }
                }
                
                respuesta.Close();
                respuesta.Dispose();

            }catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Dispose();
                CerrarConexion();
            }
            return obj;
        }

        public int Save(E_Marca obj)
        {
            int id = -1;
            SqlCommand cmd = CrearComando("marca_guardar");
            try
            {
                cmd.Parameters.AddWithValue("@id", obj.Id).Direction = ParameterDirection.InputOutput;
                cmd.Parameters.AddWithValue("@desc", obj.Descripcion);

                AbrirConexion();
                Ejecuta_Accion(ref cmd);
                id = (int)cmd.Parameters["@id"].Value;

            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Dispose();
                CerrarConexion();
            }
            return id;
        }

        public Boolean Delete(int id)
        {
            bool r = false;
            SqlCommand cmd = CrearComando("marca_borrar");
            try
            {
                cmd.Parameters.AddWithValue("@id", id);
                AbrirConexion();
                Ejecuta_Accion(ref cmd);
                r = true;
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Dispose();
                CerrarConexion();
            }

            return r;
        }

    }
}
