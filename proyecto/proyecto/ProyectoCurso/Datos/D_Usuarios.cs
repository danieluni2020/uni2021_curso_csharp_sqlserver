﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;


namespace Datos
{
    public class D_Usuarios : CAD
    {
        public Boolean Login(string usr,string pss)
        {
            Boolean r=false;
            SqlCommand cmd = CrearComando("login");
            try
            {
                cmd.Parameters.AddWithValue("@username", usr);
                cmd.Parameters.AddWithValue("@pass", pss);

                AbrirConexion();
                SqlDataReader l = Ejecuta_Consulta(cmd);
                if (l.Read())
                {
                    if (l.HasRows)
                    {
                        r = true;
                    }
                }
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Dispose();
                CerrarConexion();
            }
            return r;
        }
    }
}
