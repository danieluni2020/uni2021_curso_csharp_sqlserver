﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SistemaProyectoUNI
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void optMarca_Click(object sender, EventArgs e)
        {
            frmMarca frm = new frmMarca();
            frm.MdiParent = this;
            frm.Show();
        }
    }
}
