﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Entidades;

namespace SistemaProyectoUNI
{
    public partial class frmMarca : Form
    {
        P_Marca pMarca = new P_Marca();
        public void initGrd()
        {
            try
            {
                grd.DataSource = pMarca.GetAll().Tables[0];
                grd.Refresh();
            }catch(Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
        public frmMarca()
        {
            InitializeComponent();
        }

        private void frmMarca_Load(object sender, EventArgs e)
        {
            initGrd();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            // MessageBox.Show("Hizo Click");
            try
            {
                int vId = int.Parse(txtId.Text);
                E_Marca obj = pMarca.GetOne(vId);
                if (obj.Id == 0)
                {
                    MessageBox.Show("Id No Existe");
                    this.txtId.Text = "";
                }
                txtDesc.Text = obj.Descripcion;
            }catch(FormatException) {
                MessageBox.Show("Digite sólo valores enteros y positivos");
            }catch(Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                int vId = int.Parse(txtId.Text);
                string vDesc = txtDesc.Text;

                if (vDesc == "")
                {
                    MessageBox.Show("Descripción es Requerida");
                    txtDesc.Focus();
                    return;
                }

                E_Marca obj = new E_Marca(vId, vDesc);
                vId = pMarca.Save(obj);
                if (vId <= 0)
                {
                    MessageBox.Show("No se guardó el registro");
                }
                else
                {
                    this.txtId.Text = vId.ToString();
                    MessageBox.Show("Registro Guardado Satisfactoriamente", "Save",MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                initGrd();
            }
            catch (FormatException)
            {
                MessageBox.Show("Id debe ser entero positivo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch(Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                int vId = int.Parse(txtId.Text);
                if( MessageBox.Show("¿Borrar Marca?","Confirmación",MessageBoxButtons.YesNo,MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }

                if (pMarca.Delete(vId))
                {
                    MessageBox.Show("Borrado Satisfactoriamente");
                }
                else
                {
                    MessageBox.Show("No Pude Borrar Registro");
                }

            }
            catch (FormatException)
            {
                MessageBox.Show("Id debe ser entero positivo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                initGrd();
            }

        }
    }
}
