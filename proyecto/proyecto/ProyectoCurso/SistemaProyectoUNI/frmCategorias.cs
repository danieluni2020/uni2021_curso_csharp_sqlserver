﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Entidades;

namespace SistemaProyectoUNI
{
    public partial class frmCategorias : Form
    {
        P_Categorias pc = new P_Categorias();

        public void iniGrd()
        {
            grd.DataSource = pc.GetAll().Tables[0];
            grd.Refresh();
        }
        public frmCategorias()
        {
            InitializeComponent();
        }

        private void frmCategorias_Load(object sender, EventArgs e)
        {
            iniGrd();
        }

        private void txtId_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                int id = int.Parse(txtId.Text);
                E_Categorias o = pc.GetOne(id);

                txtId.Text = o.Id.ToString();
                txtDesc.Text = o.Descripcion;
                chkActivo.Checked = o.Activo;
                //txtUC.Text = o.Usuario_Crea;
                //txtUM.Text = o.Usuario_Modifica;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                int id = int.Parse(txtId.Text);
                E_Categorias o = new E_Categorias(id,txtDesc.Text,chkActivo.Checked,txtUC.Text,txtUM.Text);

                id = pc.Save(o);
                if (id > 0)
                {
                    MessageBox.Show("Guardado Satisfactoriamente");
                }
                else
                {
                    MessageBox.Show("Error Guardando Categoría");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                iniGrd();
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("¿Cerrar?","Confirmar",MessageBoxButtons.YesNo,MessageBoxIcon.Question)== DialogResult.Yes)
            {
                this.Close();
            }
            
        }
    }
}
