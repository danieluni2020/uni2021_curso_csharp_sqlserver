﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SistemaProyectoUNI
{
    public partial class menuPrincipal : Form
    {
        public menuPrincipal()
        {
            InitializeComponent();
        }

        private void opcMarca_Click(object sender, EventArgs e)
        {
            frmMarca frm = new frmMarca();
            frm.MdiParent = this;
            frm.Show();
        }

        private void opcCategoria_Click(object sender, EventArgs e)
        {
            frmCategorias frm = new frmCategorias();
            frm.MdiParent = this;
            frm.Show();
        }
    }
}
