﻿
namespace SistemaProyectoUNI
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.opcCatalogos = new System.Windows.Forms.ToolStripMenuItem();
            this.opcMarca = new System.Windows.Forms.ToolStripMenuItem();
            this.opcCategorias = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opcCatalogos});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // opcCatalogos
            // 
            this.opcCatalogos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opcMarca,
            this.opcCategorias});
            this.opcCatalogos.Name = "opcCatalogos";
            this.opcCatalogos.Size = new System.Drawing.Size(81, 20);
            this.opcCatalogos.Text = "Catalálogos";
            // 
            // opcMarca
            // 
            this.opcMarca.Image = global::SistemaProyectoUNI.Properties.Resources._1486503759_book_bookmark_education_school_favorite_mark_81266;
            this.opcMarca.Name = "opcMarca";
            this.opcMarca.Size = new System.Drawing.Size(130, 22);
            this.opcMarca.Text = "&Marca";
            this.opcMarca.Click += new System.EventHandler(this.opcMarca_Click);
            // 
            // opcCategorias
            // 
            this.opcCategorias.Image = global::SistemaProyectoUNI.Properties.Resources.category_add_button_icon_icons_com_71724;
            this.opcCategorias.Name = "opcCategorias";
            this.opcCategorias.Size = new System.Drawing.Size(130, 22);
            this.opcCategorias.Text = "C&ategorías";
            this.opcCategorias.Click += new System.EventHandler(this.opcCategorias_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "Proyecto final Curso Desarrollo CSharp con SQL Server";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem opcCatalogos;
        private System.Windows.Forms.ToolStripMenuItem opcMarca;
        private System.Windows.Forms.ToolStripMenuItem opcCategorias;
    }
}