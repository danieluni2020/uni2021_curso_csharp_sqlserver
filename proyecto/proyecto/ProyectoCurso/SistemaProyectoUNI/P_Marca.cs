﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using Negocios;
using Entidades;

namespace SistemaProyectoUNI
{
    public class P_Marca
    {
        N_Marca nm = new N_Marca();

        public DataSet GetAll()
        {
            return nm.GetAll();
        }
        public E_Marca GetOne(int id)
        {
            return nm.GetOne(id);
        }
        public int Save(E_Marca o)
        {
            return nm.Save(o);
        }
        public Boolean Delete(int id)
        {
            return nm.Delete(id);
        }
    }
}
