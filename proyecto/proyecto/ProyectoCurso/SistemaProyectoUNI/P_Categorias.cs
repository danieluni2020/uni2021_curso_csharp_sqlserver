﻿using System;
using System.Collections.Generic;
using System.Text;

using Negocios;
using System.Data;
using Entidades;

namespace SistemaProyectoUNI
{
    class P_Categorias
    {
        N_Categorias nc = new N_Categorias();
        public DataSet GetAll()
        {
            return nc.GetAll();
        }
        public E_Categorias GetOne(int id)
        {
            return nc.GetOne(id);
        }
        public int Save(E_Categorias o)
        {
            return nc.Save(o);
        }
    }
}
