﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SistemaProyectoUNI
{
    public partial class frmLogin : Form
    {
        P_Usuarios o = new P_Usuarios();
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string usr, pss;
            usr = txtUser.Text;
            pss = txtPass.Text;

            if (o.Login(usr, pss))
            {
                frmMain frm = new frmMain();
                frm.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Usuario/Contraseña Incorrectos");
            }
        }
    }
}
