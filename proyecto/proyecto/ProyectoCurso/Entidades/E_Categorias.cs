﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class E_Categorias
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public Boolean Activo { get; set; }
        public string Usuario_Crea { get; set; }
        public DateTime Fecha_Crea { get; set; }
        public string Usuario_Modifica { get; set; }
        public DateTime Fecha_Modifica { get; set; }

        public E_Categorias()
        {
            this.Id = 0;
            this.Descripcion = "";
            this.Activo = false;
            this.Usuario_Crea = "";
            this.Usuario_Modifica = "";
        }
        public E_Categorias(int id, string desc,Boolean activo,string uc,string um = "") 
        {
            this.Id = id;
            this.Descripcion = desc;
            this.Activo = activo;
            this.Usuario_Crea = uc;
            this.Usuario_Modifica = um;
        }
    }
}
