﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class E_Marca
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }

        public E_Marca()
        {
            this.Id = 0;
            this.Descripcion = "";
        }
        public E_Marca(int id,string desc)
        {
            this.Id = id;
            this.Descripcion = desc;
        }
    }
}
