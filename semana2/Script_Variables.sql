use Northwind
go
DECLARE @EmpID varchar(11),@vlName char(20)
SET @vlName = 'Dodsworth'
print(@vlName)
SELECT 
	@EmpID = employeeid 
	--EmployeeID
	FROM Employees
	WHERE LastName = @vlName

print(@EmpID)
SELECT @EmpID as EmpleadoId,@vlName as Nombre

--select EmployeeID,LastName from Employees where EmployeeID=5
GO
--Declarar Variables
DECLARE @Id int,@Apellido nvarchar(20)
--Inicializamos el id
SET @Id = 5
print(@id)
--Buscamos el Nombre del Empleado
SELECT @Apellido = LastName
	FROM Employees AS E
	WHERE EmployeeID=@Id

--SELECT @id as IdEmpleado,@Apellido as Apellido
DECLARE @Saludo nvarchar(100)
SET @Saludo = 'Hola Empleado ' + @Apellido
DECLARE @Saludo2 nvarchar(100)
SELECT @Saludo2 = @Apellido + ', Bienvenido...'
print(@Saludo)
print(@Saludo2)
select @Saludo as saludo1,@Saludo2 as saludo2
