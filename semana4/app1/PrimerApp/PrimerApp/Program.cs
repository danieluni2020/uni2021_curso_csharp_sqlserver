﻿using System;

namespace PrimerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Console.WriteLine("Hello World!");
            Console.ReadKey();
            */
            //Declarar Variables
            int costo;
            int precio, valor;

            // Inicializar las Variables
            costo = 50;
            valor = 10;
            precio = 35;

            //Inicializar Impuesto
            int impuesto = 10;

            Console.WriteLine("Imprimir un valor (costo={0})", costo);
            Console.WriteLine("Imprimir dos valores: Valor={0},Precio = {1}", valor, precio);

            //Imprimir dos veces una variable
            Console.WriteLine("Imprimir dos veces un valor {0} y {0}", valor);

            /* No nos olvidemos del impueseto*/
            Console.WriteLine("Impuesto {0}", impuesto);


        }
    }
}
