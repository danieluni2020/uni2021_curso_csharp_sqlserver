﻿using System;

namespace Seman4_Tarea_IF
{
    class Program
    {
        static void Main(string[] args)
        {
            int sueldo;
            sueldo = 1000;
            if (sueldo >= 3000)
            {
                Console.WriteLine("Debe pagar Impuesto");
            }
            else
            {
                Console.WriteLine("Excento de Impuesto");
            }
        }
    }
}
