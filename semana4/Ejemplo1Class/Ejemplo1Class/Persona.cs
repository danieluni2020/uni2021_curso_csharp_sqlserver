﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo1Class
{
    class Persona
    {
        private string nombre;
        private int edad;

        public void Inicializar()
        {
            Console.Write("Nombre: ");
            this.nombre = Console.ReadLine();
            Console.Write("Edad: ");
            string linea = Console.ReadLine();
            this.edad = int.Parse(linea);
        }
        public void Imprimir()
        {
            Console.WriteLine("Nombre: {0} Edad: {1}", this.nombre, this.edad);
        }
    }
}
