﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo1Class
{
    class Triangulo
    {
        private int lado1;
        private int lado2;
        private int lado3;
        private int area;

        public void Inicializar()
        {
            Console.Write("Lado 1 = ");
            this.lado1 = int.Parse(Console.ReadLine());
            Console.Write("Lado 2 = ");
            this.lado2 = int.Parse(Console.ReadLine());
            Console.Write("Lado 3 = ");
            this.lado3 = int.Parse(Console.ReadLine());
        }
        public void LadoMayor()
        {
            int lado_mayor = 0;
            string lado;
            bool equilatero = true;
            if(this.lado1 > this.lado2)
            {
                lado_mayor = this.lado1;
                lado = "lado1";
                equilatero = false;
            }
            else
            {
                lado_mayor = this.lado2;
                lado = "lado2";
            }

            if (this.lado3 > lado_mayor)
            {
                lado_mayor = this.lado3;
                lado = "lado3";
                equilatero = false;
            }
            if (equilatero)
            {
                Console.WriteLine("Sus lados son iguales a {0}", lado_mayor);
            }
            else
            {
                Console.WriteLine
                    ("El lado mayor mide {0} y fue el lado {1}", lado_mayor, lado);
            }
            
        }
        public void Tipo()
        {
            /*if(this.lado1==this.lado2 && this.lado2 == this.lado3)
            {
                Console.WriteLine("Es un triángulo equilátero");
            }
            else
            {
                Console.WriteLine("NO Es un triángulo equilátero");
            }*/
            string tipo = "NO Es un triángulo equilátero";
            if (this.lado1 == this.lado2)
            {
                if (this.lado2 == this.lado3)
                {
                    tipo = "Es un triángulo equilátero";
                }
            }
            Console.WriteLine(tipo);
        }

        private int CalcularArea()
        {
            this.area = this.lado1 + this.lado2 + this.lado3;
            return (this.area);
        }

        public Triangulo()
        {
            this.lado1 = 0;
            this.lado2 = 0;
            this.lado3 = 0;
            this.CalcularArea();
            
        }

        public Triangulo(int l1,int l2, int l3)
        {
            this.lado1 = l1;
            this.lado2 = l2;
            this.lado3 = l3;
            Console.WriteLine("El área es {0}", this.CalcularArea());
        }
    }
}
