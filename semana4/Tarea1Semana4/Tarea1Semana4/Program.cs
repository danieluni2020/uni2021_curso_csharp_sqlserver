﻿using System;

namespace Tarea1Semana4
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Realizar un programa en modo consola, donde se inicialicen dos variables de tipo entera con nombre a y b.
            Asignarle valores enteros (por ejemplo, 15 y 25)
            */
            int a, b;
            a = 15;
            b = 25;

            //Realizar las siguientes operaciones aritméticas + - * / %
            int c;

            //Sumar
            c = a + b;
            Console.WriteLine("{1} + {2} = {0}", c, a, b);

            //Resta
            c = a - b;
            Console.WriteLine("{0} - {1} = {2}", a, b, c);

            //Multiplicación
            c = a * b;
            Console.WriteLine("{0} * {1} = {2}", a, b, c);

            //División
            c = a / b;
            Console.WriteLine("{0} / {1} = {2}", a, b, c);

            //Módulo
            c = a % b;
            Console.WriteLine("{0} % {1} = {2}", a, b, c);
        }
    }
}
