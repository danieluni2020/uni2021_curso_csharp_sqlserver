--Expresiones

SELECT  OrderID, ProductID
       ,(UnitPrice * Quantity) as ExtendedAmount
 FROM  [Order Details]
 WHERE (UnitPrice * Quantity) > 10000

 
 --Control de Flujos
DECLARE @n tinyint
SET @n = 10
IF (@n BETWEEN 4 and 6)
 BEGIN  --Begin If
  WHILE @n > 0
   BEGIN  --Begin While
    /*SELECT  @n AS 'Number'
      ,CASE
        WHEN (@n % 2) = 1
          THEN 'IMPAR'
        ELSE 'PAR'
       END AS 'Tipo'*/
	print(@n)
	IF @n % 2 = 1
	BEGIN
		PRINT('Impar')
	END
	ELSE
	BEGIN
		PRINT('Par')
	END
    SET @n = @n - 1
   END  --End While
 END  --End If
ELSE
 PRINT 'FUERA DE RANGO'
 print('Esto es otra impresión')
GO
